# Pacman-Bot(s)

## Link

[BotZone](http://botzone.org/)

## Index

*   `Sample`
*   `JustForFun` 最初的梦想
*   `JustForLove` 爱上你是我情非得已
*   `SmartRandom` 豆海里的双桅船
*   `Astro Boy` 鉄腕アトム `new`

## Sample

zhouhy的Sample

## JustForFun `最初的梦想`

naive的贪心，只吃`第一个搜到`的最近的豆子+`有问题`的逃跑+`谜`一样的进攻

## JustForLove `爱上你是我情非得已`

基于豆子吸引模型+胡同。

果子对于bot有吸引力，大小为

(离我比离敌人近? 2:1) / (果子理我的最短路程)^2

并作用在所有最短路径的action方向上。

如果果子属于不安全的死胡同，就没有吸引力。

## SmartRandom `豆海里的双桅船` `c++11`

大家都在说蒙特卡洛，然而并不会。

于是默默地改了改Sample的随机，

然后出现了3个重大版本：

*   bz57
*   bz78
*   bz85
其中`bz57`和`bz78`都是Random与JustForLove的结合。`bz85`是只有随机，但随机方式有变化。

## Astro Boy `鉄腕アトム` `c++11` `new`

### 希望能在有生之年见证聪明而拥有情感的机器人诞生。 

### 实现方法

Monte—Carlo Tree Search with Upper Confidence Bound (UCT + MCTS)