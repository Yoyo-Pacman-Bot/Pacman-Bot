/*
 * Pacman 样例程序
 * 作者：zhouhy
 * 时间：2016/3/22 15:32:51
 * 最后更新：2016/5/1 2:18
 * 【更新内容3】
 * 相应改变决策部分的积分，使决策更加机智（
 * 【更新内容2】
 * 取消了对豆子产生器不合法移动的判断（即 ActionValid 函数）
 * 【更新内容】
 * 修复了文件不存在时不能通过控制台输入的Bug……
 * 修改的部位：包含了fstream库、ReadInput的函数体中前几行发生了变化，不使用freopen了。
 *
 * 【命名惯例】
 *  r/R/y/Y：Row，行，纵坐标
 *  c/C/x/X：Column，列，横坐标
 *  数组的下标都是[y][x]或[r][c]的顺序
 *  玩家编号0123
 *
 * 【坐标系】
 *   0 1 2 3 4 5 6 7 8
 * 0 +----------------> x
 * 1 |
 * 2 |
 * 3 |
 * 4 |
 * 5 |
 * 6 |
 * 7 |
 * 8 |
 *   v y
 *
 * 【提示】你可以使用
 * #ifndef _BOTZONE_ONLINE
 * 这样的预编译指令来区分在线评测和本地评测
 *
 * 【提示】一般的文本编辑器都会支持将代码块折叠起来
 * 如果你觉得自带代码太过冗长，可以考虑将整个namespace折叠
 */

// #define _DEBUG_CRY

#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
#include <stack>
#include <stdexcept>
#include <random>
#include <cfloat>
#include "jsoncpp/json.h"

// #define FIELD_MAX_HEIGHT 20
#define FIELD_MAX_HEIGHT 11
// #define FIELD_MAX_WIDTH 20
#define FIELD_MAX_WIDTH 11
#define MAX_GENERATOR_COUNT 4 // 每个象限1
#define MAX_PLAYER_COUNT 4
#define MAX_TURN 100

// 随机深度 f(turnID)
// #define RANDOM_TURN_MAX 10
// 随机次数块 （总次数）
// 随机退出时间 ms
#define RANDOM_BLOCK_1 9000
#define RANDOM_CLOCK_1 700
#define RANDOM_BLOCK_2 1000
#define RANDOM_CLOCK_2 950

 #ifndef _BOTZONE_ONLINE
#include <iomanip>
 #endif

// PacBot : : base64
/**
* @author   Yoyo
* @date	 start 2016/04/26
*		   last  2016/04/28
* @description
*		   base64 encoder and decoder
*/
#ifndef BASE64
#define BASE64
#include <cstring>
/* header */
namespace base64 {

	/**
	 * encode base64 (base83 to base64)
	 * @param  source the source , without '\0' in the end, base83
	 * @param  len	the length(size) of the source.
	 * @return		the c string head point, base64
	 *				the string has '\0' end tag.
	 */
	char * encode(const char * source, int len);
	/**
	 * encode base64 (base83 to base64)
	 * @param  source the source c string, base83
	 * @return		the c string head point, base64
	 *				the string has '\0' end tag.
	 */
	char * encode(const char * source);

	/**
	 * decode base64 (base64 to base83)
	 * @param  source the source c string, base 64
	 * @return		the c string head point, base 83
	 *				the string has '\0' end tag.
	 */
	char * decode(const char * source);

	/**
	 * decode base64 (base64 to base83)
	 * @param  source the source c string, base 64
	 * @param  dest   the target point. !!! should be careful !!!. won't add '\0' in the end.
	 * @return		succeed, return true;
	 */
	bool decode(const char * source, char * dest);
}
/* source */
namespace base64 {
	const char * encoding = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
					/* ascii 43 */
	const char decoding[] = {62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-2,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51};

	char * encode(const char * source) {
		return encode(source, strlen(source));
	}

	char * encode(const char * source, int len) {
		if (len <= 0) {
			return NULL;
		}
		unsigned int n = (len + 2) / 3;
		unsigned int m = 3 * n - len;
		unsigned int len_dest = 4 * n + 1;
		char * dest = new char[len_dest];
		char * dest_p = dest;
		const char * source_p = source;
		for (int i = 1; i < n; ++i) { // n - 1 times
			char buffer_s[3];
			char buffer_d[4];
			memcpy(buffer_s, source_p, 3);

			buffer_d[0] = encoding[(buffer_s[0] >> 2) & 0x3f];
			buffer_d[1] = encoding[((buffer_s[0] << 4) & 0x30) | ((buffer_s[1] >> 4) & 0x0f)];
			buffer_d[2] = encoding[((buffer_s[1] << 2) & 0x3c) | ((buffer_s[2] >> 6) & 0x03)];
			buffer_d[3] = encoding[buffer_s[2] & 0x3f];

			memcpy(dest_p, buffer_d, 4);
			source_p += 3;
			dest_p += 4;
		}
		char buffer_s[3];
		char buffer_d[4];
		memset(buffer_s, 0, 3);
		memcpy(buffer_s, source_p, 3 - m);

		buffer_d[0] = encoding[(buffer_s[0] >> 2) & 0x3f];
		buffer_d[1] = encoding[((buffer_s[0] << 4) & 0x30) | ((buffer_s[1] >> 4) & 0x0f)];
		buffer_d[2] = encoding[((buffer_s[1] << 2) & 0x3c) | ((buffer_s[2] >> 6) & 0x03)];
		buffer_d[3] = encoding[buffer_s[2] & 0x3f];

		for (int i = 0; i < m; ++i) {
			buffer_d[3 - i] = '=';
		}

		memcpy(dest_p, buffer_d, 4);
		dest_p += 4;
		*dest_p = 0;
		return dest;
	}

	bool decode(const char * source, char * dest) {
		unsigned int len = strlen(source);
		if (len == 0) {
			return false;
		}
		unsigned int n = len >> 2;
		unsigned int m = 0;
		const char * temp = strchr(source, '=');
		while (temp) {
			++temp;
			temp = strchr(temp, '=');
			++m;
		}

		char * dest_p = dest;
	   
			   const char * source_p = source;
		for (int i = 1; i < n; ++i) { // n - 1 times
			char buffer_s[4];
			char buffer_d[3];
			memcpy(buffer_s, source_p, 4);

			buffer_s[0] = decoding[buffer_s[0] - 43];
			buffer_s[1] = decoding[buffer_s[1] - 43];
			buffer_s[2] = decoding[buffer_s[2] - 43];
			buffer_s[3] = decoding[buffer_s[3] - 43];

			buffer_d[0] = ((buffer_s[0] << 2) & 0xfc) | ((buffer_s[1] >> 4) & 0x03);
			buffer_d[1] = ((buffer_s[1] << 4) & 0xf0) | ((buffer_s[2] >> 2) & 0x0f);
			buffer_d[2] = ((buffer_s[2] << 6) & 0xc0) | (buffer_s[3] & 0x3f);

			memcpy(dest_p, buffer_d, 3);
			source_p += 4;
			dest_p += 3;
		}

		char buffer_s[4];
		char buffer_d[3];
		memset(buffer_s, 0, 4);
		memcpy(buffer_s, source_p, strlen(source_p));

		buffer_s[0] = decoding[buffer_s[0] - 43];
		buffer_s[1] = decoding[buffer_s[1] - 43];
		buffer_d[0] = ((buffer_s[0] << 2) & 0xfc) | ((buffer_s[1] >> 4) & 0x03);
		if (m < 2) {
		buffer_s[2] = decoding[buffer_s[2] - 43];
		buffer_d[1] = ((buffer_s[1] << 4) & 0xf0) | ((buffer_s[2] >> 2) & 0x0f);
		}
		if (m < 1) {
		buffer_s[3] = decoding[buffer_s[3] - 43];
		buffer_d[2] = ((buffer_s[2] << 6) & 0xc0) | (buffer_s[3] & 0x3f);
		}

		memcpy(dest_p, buffer_d, 3 - m);
		return true;
	}

	char * decode(const char * source) {
		unsigned int len = strlen(source);
		unsigned int n = len >> 2;
		unsigned int m = 0;
		const char * temp = strchr(source, '=');
		while (temp) {
			++temp;
			temp = strchr(temp, '=');
			++m;
		}

		char * dest = new char[3 * n - m];
	   
			   decode(source, dest);
		*(dest + 3 * n - m) = '\0';
		return dest;
	}
}
#endif /* BASE64 */

// PacBot : : bot
/**
* @author   Yoyo
* @date	 start 2016/04/27
*		   last  2016/05/01 add json type
* @description
*		   IO for Bot data & globleData
*/
/* header */
namespace bot {

	std::string strData, strGlobalData; // 这是回合之间可以传递的信息
	Json::Value Debug;
	class CJsonIO
	{
	public:

		bool Read();

		bool Write();

		bool IsExist(const std::string & Key);

		/**
		 * data 应当是指针类型！！！
		 * 内存极不安全，确保Get与Set的size一致！！！
		 */
		template <typename Data_type>
		bool GetBase64(const std::string & Key, Data_type data);

		/**
		 * data 应当是指针类型！！！
		 */
		template <typename Data_type>
		void SetBase64(const std::string & Key, const Data_type data, unsigned int size);

		bool GetString(const std::string & Key, std::string & data);

		void SetString(const std::string & Key, const std::string & data);

		bool GetJson(const std::string & Key, Json::Value & json);

		void SetJson(const std::string & Key, const Json::Value & json);

		void RemoveKey(const std::string & Key);

		void Clear();

		CJsonIO(std::string & str);
		~CJsonIO();
	   
		   private:
		std::string & m_str;
		Json::Value m_root;
		Json::FastWriter m_writer;
		Json::Reader m_reader;

	} data(strData), globalData(strGlobalData);
}
/* source */
namespace bot {
	CJsonIO::CJsonIO(std::string & str) : m_str(str) {}
	CJsonIO::~CJsonIO() {}
	inline bool CJsonIO::Read() {
		return m_reader.parse(m_str, m_root, false);
	}
	inline bool CJsonIO::Write() {
		std::string temp = m_writer.write(m_root);
		if(temp.length() > 1e5) {
			return false;
		}
		m_str = temp;
		return true;
	}
	inline bool CJsonIO::IsExist(const std::string & Key) {
		return m_root.isMember(Key);
	}
	template <typename Data_type>
	inline bool CJsonIO::GetBase64(const std::string & Key, Data_type data) {
		if (m_root.isMember(Key) == false) {
			return false;
		}
		return base64::decode(m_root[Key].asCString(), (char *)data);
	}

	template <typename Data_type>
	inline void CJsonIO::SetBase64(const std::string & Key, const Data_type data, unsigned int size) {
		char * ans = base64::encode((char *)data, size);
		m_root[Key] = Json::Value(ans);
		delete [] ans;
	}

	inline bool CJsonIO::GetString(const std::string & Key, std::string & data) {
		if (m_root.isMember(Key) == false) {
			return false;
		}
		data = m_root[Key].asString();
		return true;
	}
	inline void CJsonIO::SetString(const std::string & Key, const std::string & data) {
		m_root[Key] = Json::Value(data);
	}
	inline bool CJsonIO::GetJson(const std::string & Key, Json::Value & json) {
		if (m_root.isMember(Key) == false) {
			return false;
		}
		json = m_root[Key];
		return true;
	}
	inline void CJsonIO::SetJson(const std::string & Key, const Json::Value & json) {
		m_root[Key] = json;
	}
	inline void CJsonIO::RemoveKey(const std::string & Key) {
		m_root.removeMember(Key);
	}
	inline void CJsonIO::Clear() {
		m_root.clear();
	}
}

// 你也可以选用 using namespace std; 但是会污染命名空间
using std::string;
using std::swap;
using std::cin;
using std::cout;
using std::endl;
using std::getline;
using std::runtime_error;

namespace SmartRandom {
	inline void EdgeCorrect(int & i, int & j, const int & h, const int & w) {
		if (i == -1) {
			i = h - 1;
		}
		else if (i == h) {
			i = 0;
		}
		if (j == -1) {
			j = w - 1;
		}
		else if (j == w) {
			j = 0;
		}
		return;
	}
}

// 平台提供的吃豆人相关逻辑处理程序
namespace Pacman
{
	const time_t seed = time(0);
	const int dx[] = { 0, 1, 0, -1, 1, 1, -1, -1 }, dy[] = { -1, 0, 1, 0, -1, 1, 1, -1 };

	// PacBot : : path typedef
	typedef short GridDistanceType; // 用于fieldDistance[s_i][s_j][d_i][d_j]
	typedef char  GridPathType;	 // 用于	fieldPath[s_i][s_j][d_i][d_j]
	typedef char  GridActionType;  

	// 枚举定义；使用枚举虽然会浪费空间（sizeof(GridContentType) == 4），但是计算机处理32位的数字效率更高

	// 每个格子可能变化的内容，会采用“或”逻辑进行组合
	enum GridContentType
	{
		empty = 0, // 其实不会用到
		player1 = 1, // 1号玩家
		player2 = 2, // 2号玩家
		player3 = 4, // 3号玩家
		player4 = 8, // 4号玩家
		playerMask = 1 | 2 | 4 | 8, // 用于检查有没有玩家等
		smallFruit = 16, // 小豆子
		largeFruit = 32 // 大豆子
	};

	// 用玩家ID换取格子上玩家的二进制位
	GridContentType playerID2Mask[] = { player1, player2, player3, player4 };
	string playerID2str[] = { "0", "1", "2", "3" };

	// 让枚举也可以用这些运算了（不加会编译错误）
	template<typename T>
	inline T operator |=(T &a, const T &b)
	{
		return a = static_cast<T>(static_cast<int>(a) | static_cast<int>(b));
	}
	template<typename T>
	inline T operator |(const T &a, const T &b)
	{
		return static_cast<T>(static_cast<int>(a) | static_cast<int>(b));
	}
	template<typename T>
	inline T operator &=(T &a, const T &b)
	{
		return a = static_cast<T>(static_cast<int>(a) & static_cast<int>(b));
	}
	template<typename T>
	inline T operator &(const T &a, const T &b)
	{
		return static_cast<T>(static_cast<int>(a) & static_cast<int>(b));
	}
	template<typename T>
	inline T operator ++(T &a)
	{
		return a = static_cast<T>(static_cast<int>(a) + 1);
	}
	template<typename T>
	inline T operator ~(const T &a)
	{
		return static_cast<T>(~static_cast<int>(a));
	}

	// 每个格子固定的东西，会采用“或”逻辑进行组合
	enum GridStaticType
	{
		emptyWall = 0, // 其实不会用到
		wallNorth = 1, // 北墙（纵坐标减少的方向）
		wallEast = 2, // 东墙（横坐标增加的方向）
		wallSouth = 4, // 南墙（纵坐标增加的方向）
		wallWest = 8, // 西墙（横坐标减少的方向）
		generator = 16 // 豆子产生器
	};

	// 用移动方向换取这个方向上阻挡着的墙的二进制位
	GridStaticType direction2OpposingWall[] = { wallNorth, wallEast, wallSouth, wallWest };

	// 方向，可以代入dx、dy数组，同时也可以作为玩家的动作
	enum Direction
	{
		stay = -1,
		up = 0,
		right = 1,
		down = 2,
		left = 3,
		// 下面的这几个只是为了产生器程序方便，不会实际用到
		ur = 4, // 右上
		dr = 5, // 右下
		dl = 6, // 左下
		ul = 7 // 左上
	};

	// 场地上带有坐标的物件
	struct FieldProp
	{
		int row, col;
	};

	// 场地上的玩家
	struct Player : FieldProp
	{
		int strength;
		int powerUpLeft;
		bool dead;
	};

	// 回合新产生的豆子的坐标
	struct NewFruits
	{
		FieldProp newFruits[MAX_GENERATOR_COUNT * 8];
		int newFruitCount;
	} newFruits[MAX_TURN];
	int newFruitsCount = 0;

	// 状态转移记录结构
	struct TurnStateTransfer
	{
		enum StatusChange // 可组合
		{
			none = 0,
			ateSmall = 1,
			ateLarge = 2,
			powerUpCancel = 4,
			die = 8,
			error = 16
		};

		// 玩家选定的动作
		Direction actions[MAX_PLAYER_COUNT];

		// 此回合该玩家的状态变化
		StatusChange change[MAX_PLAYER_COUNT];

		// 此回合该玩家的力量变化
		int strengthDelta[MAX_PLAYER_COUNT];
	};

	// 游戏主要逻辑处理类，包括输入输出、回合演算、状态转移，全局唯一
	class GameField
	{
	private:
		// 为了方便，大多数属性都不是private的

		// 记录每回合的变化（栈）
		TurnStateTransfer backtrack[MAX_TURN];

		// 这个对象是否已经创建
		static bool constructed;

	public:
		// 场地的长和宽
		int height, width;
		int generatorCount;
		int GENERATOR_INTERVAL, LARGE_FRUIT_DURATION, LARGE_FRUIT_ENHANCEMENT;

		// 场地格子固定的内容
		GridStaticType fieldStatic[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];

		// 场地格子会变化的内容
		GridContentType fieldContent[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];

		// PacBot : : path Grid declare
		GridDistanceType fieldDistance[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];
			GridPathType	 fieldPath[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];
		  GridActionType   fieldAction[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];

		int generatorTurnLeft; // 多少回合后产生豆子
		int aliveCount; // 有多少玩家存活
		int smallFruitCount;
		int turnID;
		FieldProp generators[MAX_GENERATOR_COUNT]; // 有哪些豆子产生器
		Player players[MAX_PLAYER_COUNT]; // 有哪些玩家

		// 玩家选定的动作
		Direction actions[MAX_PLAYER_COUNT];

		// 恢复到上次场地状态。可以一路恢复到最开始。
		// 恢复失败（没有状态可恢复）返回false
		bool PopState()
		{
			if (turnID <= 0)
				return false;

			const TurnStateTransfer &bt = backtrack[--turnID];
			int i, _;

			// 倒着来恢复状态

			for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				Player &_p = players[_];
				GridContentType &content = fieldContent[_p.row][_p.col];
				TurnStateTransfer::StatusChange change = bt.change[_];

				if (!_p.dead)
				{
					// 5. 大豆回合恢复
					if (_p.powerUpLeft || change & TurnStateTransfer::powerUpCancel)
						_p.powerUpLeft++;

					// 4. 吐出豆子
					if (change & TurnStateTransfer::ateSmall)
					{
						content |= smallFruit;
						smallFruitCount++;
					}
					else if (change & TurnStateTransfer::ateLarge)
					{
						content |= largeFruit;
						_p.powerUpLeft -= LARGE_FRUIT_DURATION;
					}
				}

				// 2. 魂兮归来
				if (change & TurnStateTransfer::die)
				{
					_p.dead = false;
					aliveCount++;
					content |= playerID2Mask[_];
				}

				// 1. 移形换影
				if (!_p.dead && bt.actions[_] != stay)
				{
					fieldContent[_p.row][_p.col] &= ~playerID2Mask[_];
					_p.row = (_p.row - dy[bt.actions[_]] + height) % height;
					_p.col = (_p.col - dx[bt.actions[_]] + width) % width;
					fieldContent[_p.row][_p.col] |= playerID2Mask[_];
				}

				// 0. 救赎不合法的灵魂
				if (change & TurnStateTransfer::error)
				{
					_p.dead = false;
					aliveCount++;
					content |= playerID2Mask[_];
				}

				// *. 恢复力量
				if (!_p.dead)
					_p.strength -= bt.strengthDelta[_];
			}

			// 3. 收回豆子
			if (generatorTurnLeft == GENERATOR_INTERVAL)
			{
				generatorTurnLeft = 1;
				NewFruits &fruits = newFruits[--newFruitsCount];
				for (i = 0; i < fruits.newFruitCount; i++)
				{
					fieldContent[fruits.newFruits[i].row][fruits.newFruits[i].col] &= ~smallFruit;
					smallFruitCount--;
				}
			}
			else
				generatorTurnLeft++;

			return true;
		}

		// 判断指定玩家向指定方向移动是不是合法的（没有撞墙）
		inline bool ActionValid(const int playerID, const Direction &dir) const
		{
			if (dir == stay)
				return true;
			const Player &p = players[playerID];
			const GridStaticType &s = fieldStatic[p.row][p.col];
			return dir >= -1 && dir < 4 && !(s & direction2OpposingWall[dir]);
		}

		inline bool ActionValid(const int & i, const int & j, const Direction &dir) const {
			if (dir == stay)
				return true;
			const GridStaticType &s = fieldStatic[i][j];
			return dir >= -1 && dir < 4 && !(s & direction2OpposingWall[dir]);
		}

		// 在向actions写入玩家动作后，演算下一回合局面，并记录之前所有的场地状态，可供日后恢复。
		// 是终局的话就返回false
		bool NextTurn()
		{
			int _, i, j;

			TurnStateTransfer &bt = backtrack[turnID];
			memset(&bt, 0, sizeof(bt));

			// 0. 杀死不合法输入
			for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				Player &p = players[_];
				if (!p.dead)
				{
					Direction &action = actions[_];
					if (action == stay)
						continue;

					if (!ActionValid(_, action))
					{
						bt.strengthDelta[_] += -p.strength;
						bt.change[_] = TurnStateTransfer::error;
						fieldContent[p.row][p.col] &= ~playerID2Mask[_];
						p.strength = 0;
						p.dead = true;
						aliveCount--;
					}
					else
					{
						// 遇到比自己强♂壮的玩家是不能前进的
						GridContentType target = fieldContent
							[(p.row + dy[action] + height) % height]
							[(p.col + dx[action] + width) % width];
						if (target & playerMask)
							for (i = 0; i < MAX_PLAYER_COUNT; i++)
								if (target & playerID2Mask[i] && players[i].strength > p.strength)
									action = stay;
					}
				}
			}

			// 1. 位置变化
			for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				Player &_p = players[_];
				if (_p.dead)
					continue;

				bt.actions[_] = actions[_];

				if (actions[_] == stay)
					continue;

				// 移动
				fieldContent[_p.row][_p.col] &= ~playerID2Mask[_];
				_p.row = (_p.row + dy[actions[_]] + height) % height;
				_p.col = (_p.col + dx[actions[_]] + width) % width;
				fieldContent[_p.row][_p.col] |= playerID2Mask[_];
			}

			// 2. 玩家互殴
			for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				Player &_p = players[_];
				if (_p.dead)
					continue;

				// 判断是否有玩家在一起
				int player, containedCount = 0;
				int containedPlayers[MAX_PLAYER_COUNT];
				for (player = 0; player < MAX_PLAYER_COUNT; player++)
					if (fieldContent[_p.row][_p.col] & playerID2Mask[player])
						containedPlayers[containedCount++] = player;

				if (containedCount > 1)
				{
					// NAIVE
					for (i = 0; i < containedCount; i++)
						for (j = 0; j < containedCount - i - 1; j++)
							if (players[containedPlayers[j]].strength < players[containedPlayers[j + 1]].strength)
								swap(containedPlayers[j], containedPlayers[j + 1]);

					int begin;
					for (begin = 1; begin < containedCount; begin++)
						if (players[containedPlayers[begin - 1]].strength > players[containedPlayers[begin]].strength)
							break;

					// 这些玩家将会被杀死
					int lootedStrength = 0;
					for (i = begin; i < containedCount; i++)
					{
						int id = containedPlayers[i];
						Player &p = players[id];

						// 从格子上移走
						fieldContent[p.row][p.col] &= ~playerID2Mask[id];
						p.dead = true;
						int drop = p.strength / 2;
						bt.strengthDelta[id] += -drop;
						bt.change[id] |= TurnStateTransfer::die;
						lootedStrength += drop;
						p.strength -= drop;
						aliveCount--;
					}

					// 分配给其他玩家
					int inc = lootedStrength / begin;
					for (i = 0; i < begin; i++)
					{
						int id = containedPlayers[i];
						Player &p = players[id];
						bt.strengthDelta[id] += inc;
						p.strength += inc;
					}
				}
			}

			// 3. 产生豆子
			if (--generatorTurnLeft == 0)
			{
				generatorTurnLeft = GENERATOR_INTERVAL;
				NewFruits &fruits = newFruits[newFruitsCount++];
				fruits.newFruitCount = 0;
				for (i = 0; i < generatorCount; i++)
					for (Direction d = up; d < 8; ++d)
					{
						// 取余，穿过场地边界
						int r = (generators[i].row + dy[d] + height) % height, c = (generators[i].col + dx[d] + width) % width;
						if (fieldStatic[r][c] & generator || fieldContent[r][c] & (smallFruit | largeFruit))
							continue;
						fieldContent[r][c] |= smallFruit;
						fruits.newFruits[fruits.newFruitCount].row = r;
						fruits.newFruits[fruits.newFruitCount++].col = c;
						smallFruitCount++;
					}
			}

			// 4. 吃掉豆子
			for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				Player &_p = players[_];
				if (_p.dead)
					continue;

				GridContentType &content = fieldContent[_p.row][_p.col];

				// 只有在格子上只有自己的时候才能吃掉豆子
				if (content & playerMask & ~playerID2Mask[_])
					continue;

				if (content & smallFruit)
				{
					content &= ~smallFruit;
					_p.strength++;
					bt.strengthDelta[_]++;
					smallFruitCount--;
					bt.change[_] |= TurnStateTransfer::ateSmall;
				}
				else if (content & largeFruit)
				{
					content &= ~largeFruit;
					if (_p.powerUpLeft == 0)
					{
						_p.strength += LARGE_FRUIT_ENHANCEMENT;
						bt.strengthDelta[_] += LARGE_FRUIT_ENHANCEMENT;
					}
					_p.powerUpLeft += LARGE_FRUIT_DURATION;
					bt.change[_] |= TurnStateTransfer::ateLarge;
				}
			}

			// 5. 大豆回合减少
			for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				Player &_p = players[_];
				if (_p.dead)
					continue;

				if (_p.powerUpLeft > 0 && --_p.powerUpLeft == 0)
				{
					_p.strength -= LARGE_FRUIT_ENHANCEMENT;
					bt.change[_] |= TurnStateTransfer::powerUpCancel;
					bt.strengthDelta[_] += -LARGE_FRUIT_ENHANCEMENT;
				}
			}

			++turnID;

			// 是否只剩一人？
			if (aliveCount <= 1)
			{
				for (_ = 0; _ < MAX_PLAYER_COUNT; _++)
					if (!players[_].dead)
					{
						bt.strengthDelta[_] += smallFruitCount;
						players[_].strength += smallFruitCount;
					}
				return false;
			}

			// 是否回合超限？
			if (turnID >= 100)
				return false;

			return true;
		}

		// 读取并解析程序输入，本地调试或提交平台使用都可以。
		// 如果在本地调试，程序会先试着读取参数中指定的文件作为输入文件，失败后再选择等待用户直接输入。
		// 本地调试时可以接受多行以便操作，Windows下可以用 Ctrl-Z 或一个【空行+回车】表示输入结束，但是在线评测只需接受单行即可。
		// localFileName 可以为NULL
		// obtainedData 会输出自己上回合存储供本回合使用的数据
		// obtainedGlobalData 会输出自己的 Bot 上以前存储的数据
		// 返回值是自己的 playerID
		int ReadInput(const char *localFileName, string &obtainedData, string &obtainedGlobalData)
		{
			string str, chunk;
			#ifdef _BOTZONE_ONLINE
			std::ios::sync_with_stdio(false); //ω\\)
			getline(cin, str);
			#else
			if (localFileName)
			{
				std::ifstream fin(localFileName);
				if (fin)
					while (getline(fin, chunk) && chunk != "")
						str += chunk;
				else
					while (getline(cin, chunk) && chunk != "")
						str += chunk;
			}
			else
				while (getline(cin, chunk) && chunk != "")
					str += chunk;
			#endif
			Json::Reader reader;
			Json::Value input;
			reader.parse(str, input);


			// BotJson
			obtainedData = input["data"].asString();
			obtainedGlobalData = input["globaldata"].asString();
			bot::data.Read();
			bot::globalData.Read();


			int len = input["requests"].size();

			// 读取场地静态状况
			Json::Value field = input["requests"][(Json::Value::UInt) 0],
				staticField = field["static"], // 墙面和产生器
				contentField = field["content"]; // 豆子和玩家


			height = field["height"].asInt();
			width = field["width"].asInt();
			LARGE_FRUIT_DURATION = field["LARGE_FRUIT_DURATION"].asInt();
			LARGE_FRUIT_ENHANCEMENT = field["LARGE_FRUIT_ENHANCEMENT"].asInt();
			generatorTurnLeft = GENERATOR_INTERVAL = field["GENERATOR_INTERVAL"].asInt();

			PrepareInitialField(staticField, contentField);

			// 根据历史恢复局面
			for (int i = 1; i < len; i++)
			{
				Json::Value req = input["requests"][i];
				for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
					if (!players[_].dead)
						actions[_] = (Direction)req[playerID2str[_]]["action"].asInt();
				NextTurn();
			}

			return field["id"].asInt();
		}

		// 根据 static 和 content 数组准备场地的初始状况
		void PrepareInitialField(const Json::Value &staticField, const Json::Value &contentField)
		{
			int r, c, gid = 0;
			generatorCount = 0;
			aliveCount = 0;
			smallFruitCount = 0;
			generatorTurnLeft = GENERATOR_INTERVAL;
			for (r = 0; r < height; r++)
				for (c = 0; c < width; c++)
				{
					GridContentType &content = fieldContent[r][c] = (GridContentType)contentField[r][c].asInt();
					GridStaticType &s = fieldStatic[r][c] = (GridStaticType)staticField[r][c].asInt();
					if (s & generator)
					{
						generators[gid].row = r;
						generators[gid++].col = c;
						generatorCount++;
					}
					if (content & smallFruit)
						smallFruitCount++;
					for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
						if (content & playerID2Mask[_])
						{
							Player &p = players[_];
							p.col = c;
							p.row = r;
							p.powerUpLeft = 0;
							p.strength = 1;
							p.dead = false;
							aliveCount++;
						}
				}
		}

		// 完成决策，输出结果。
		// action 表示本回合的移动方向，stay 为不移动
		// tauntText 表示想要叫嚣的言语，可以是任意字符串，除了显示在屏幕上不会有任何作用，留空表示不叫嚣
		// data 表示自己想存储供下一回合使用的数据，留空表示删除
		// globalData 表示自己想存储供以后使用的数据（替换），这个数据可以跨对局使用，会一直绑定在这个 Bot 上，留空表示删除
		void WriteOutput(Direction action, string tauntText = "", string data = "", string globalData = "") const
		{
			Json::Value ret;
			ret["response"]["action"] = action;
			ret["response"]["tauntText"] = tauntText;
			ret["data"] = data;
			ret["globaldata"] = globalData;
			ret["debug"] = bot::Debug;

			#ifdef _BOTZONE_ONLINE
			Json::FastWriter writer; // 在线评测的话能用就行……
			cout << writer.write(ret) << endl;
			#endif
		}

		// 用于显示当前游戏状态，调试用。
		// 提交到平台后会被优化掉。
		inline void DebugPrint() const
		{
			#ifndef _BOTZONE_ONLINE
			printf("回合号【%d】存活人数【%d】| 图例 产生器[G] 有玩家[0/1/2/3] 多个玩家[*] 大豆[o] 小豆[.]\n", turnID, aliveCount);
			for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				const Player &p = players[_];
				printf("[玩家%d(%d, %d)|力量%d|加成剩余回合%d|%s]\n",
					_, p.row, p.col, p.strength, p.powerUpLeft, p.dead ? "死亡" : "存活");
			}
			putchar(' ');
			putchar(' ');
			for (int c = 0; c < width; c++)
				printf("  %d ", c);
			putchar('\n');
			for (int r = 0; r < height; r++)
			{
				putchar(' ');
				putchar(' ');
				for (int c = 0; c < width; c++)
				{
					putchar(' ');
					printf((fieldStatic[r][c] & wallNorth) ? "---" : "   ");
				}
				printf("\n%d ", r);
				for (int c = 0; c < width; c++)
				{
					putchar((fieldStatic[r][c] & wallWest) ? '|' : ' ');
					putchar(' ');
					int hasPlayer = -1;
					for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
						if (fieldContent[r][c] & playerID2Mask[_])
							if (hasPlayer == -1)
								hasPlayer = _;
							else
								hasPlayer = 4;
					if (hasPlayer == 4)
						putchar('*');
					else if (hasPlayer != -1)
						putchar('0' + hasPlayer);
					else if (fieldStatic[r][c] & generator)
						putchar('G');
					else if (fieldContent[r][c] & playerMask)
						putchar('*');
					else if (fieldContent[r][c] & smallFruit)
						putchar('.');
					else if (fieldContent[r][c] & largeFruit)
						putchar('o');
					else
						putchar(' ');
					putchar(' ');
				}
				putchar((fieldStatic[r][width - 1] & wallEast) ? '|' : ' ');
				putchar('\n');
			}
			putchar(' ');
			putchar(' ');
			for (int c = 0; c < width; c++)
			{
				putchar(' ');
				printf((fieldStatic[height - 1][c] & wallSouth) ? "---" : "   ");
			}
			putchar('\n');
			#endif
		}

		Json::Value SerializeCurrentTurnChange()
		{
			Json::Value result;
			TurnStateTransfer &bt = backtrack[turnID - 1];
			for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
			{
				result["actions"][_] = bt.actions[_];
				result["strengthDelta"][_] = bt.strengthDelta[_];
				result["change"][_] = bt.change[_];
			}
			return result;
		}

		// 初始化游戏管理器
		GameField()
		{
			if (constructed)
				throw runtime_error("请不要再创建 GameField 对象了，整个程序中只应该有一个对象");
			constructed = true;

			turnID = 0;
		}

		GameField(const GameField &b) : GameField() { }
	};

	bool GameField::constructed = false;
}

// PacBot : : path
/**
* @author   Yoyo
* @date	 start 2016/04/26
*		   last  2016/05/04 multi-path
* @description
*		   Dijkstra (shortest path), AStar (启发式路径搜索)(暂无)
*			   Path define
*			   0
*			   1 from right,
*			   2 from bottom,
*			   3 from left,
*			   4 stay here. (just for the first node)
*			   5 can't be approached
*/
#include <queue>
/* header */
namespace path {
	/**
	 * 全源全目标最短路径搜索
	 * 数据储存在Pacman::GameField 中的	fieldDistance[s_i][s_j][d_i][d_j]
	 *									  fieldPath[s_i][s_j][d_i][d_j]
	 * @param gameField  Pacman::GameField见namespace Pacman
	 */
	void Dijkstra(Pacman::GameField & gameField);
}
/* source */
namespace path {
	struct Node
	{
		int i;
		int j;
		int d; // fgh distance here
		int p; // path from, now defination is: 1 from bottom,
			   //							   2 from left,
			   //							   4 from top,
			   //							   8 from right,
			   //							   0 stay here. (just for the first node)
			   //							   -1 can't be approached
		const Pacman::GridActionType * a; // node from
		Node(const int & _i, const int & _j, const int & _d, const int & _p, const Pacman::GridActionType * _a) : i(_i), j(_j), d(_d), p(_p), a(_a) {}
	};
	// a > b return true
	bool operator<(const Node & a, const Node & b) {
		return a.d > b.d;
	}

	void Dijkstra(Pacman::GameField & gameField) {
		memset(gameField.fieldDistance, -1, sizeof(gameField.fieldDistance));
		memset(gameField.fieldPath, -1, sizeof(gameField.fieldPath));
		memset(gameField.fieldAction, 0, sizeof(gameField.fieldAction));
		for (int i = 0; i < gameField.height; ++i)
		{
			for (int j = 0; j < gameField.width; ++j)
			{
				std::priority_queue<Node> open;
				open.push(Node(i, j, 0, 0, &gameField.fieldAction[i][j][i][j]));
				// closed fieldPath != -1;
				while (!open.empty()) {
					Node now = open.top();
					open.pop();
					if (now.i == gameField.height) {
						now.i = 0;
					}
					if (now.j == gameField.width) {
						now.j = 0;
					}
					if (now.i == -1) {
						now.i = gameField.height - 1;
					}
					if (now.j == -1) {
						now.j = gameField.width - 1;
					}
					if (gameField.fieldDistance[i][j][now.i][now.j] == -1) {
						gameField.fieldDistance[i][j][now.i][now.j] = now.d;
						gameField.fieldPath[i][j][now.i][now.j] = now.p;
						if (*(now.a) == 0) {
							gameField.fieldAction[i][j][now.i][now.j] = now.p;
						}
						else {
							gameField.fieldAction[i][j][now.i][now.j] = *(now.a);
						}

						// 需要在gameField里面将未封闭的豆子产生器围起来！！！
						if (~gameField.fieldStatic[now.i][now.j] & 1) {
							open.push(Node(now.i - 1, now.j, now.d + 1, 1, &gameField.fieldAction[i][j][now.i][now.j]));
						}
						if (~gameField.fieldStatic[now.i][now.j] & 2) {
							open.push(Node(now.i, now.j + 1, now.d + 1, 2, &gameField.fieldAction[i][j][now.i][now.j]));
						}
						if (~gameField.fieldStatic[now.i][now.j] & 4) {
							open.push(Node(now.i + 1, now.j, now.d + 1, 4, &gameField.fieldAction[i][j][now.i][now.j]));
						}
						if (~gameField.fieldStatic[now.i][now.j] & 8) {
							open.push(Node(now.i, now.j - 1, now.d + 1, 8, &gameField.fieldAction[i][j][now.i][now.j]));
						}
					}
					else if (gameField.fieldDistance[i][j][now.i][now.j] == now.d) { // 多条路径
						gameField.fieldPath[i][j][now.i][now.j] |= now.p;
						if (*(now.a) == 0) {
							gameField.fieldAction[i][j][now.i][now.j] |= now.p;
						}
						else {
							gameField.fieldAction[i][j][now.i][now.j] |= *(now.a);
						}
					}
				}
			}
		}
	}
}

// Hutong
#include <queue>
#include <cstring>
// 接口部分
namespace Hutong {
	void Hutong(Pacman::GameField & gameField);
	struct _GATE {
		int i;
		int j;
		_GATE() {i = -1; j = -1;}
	};
	struct _POINT
	{
		int i;
		int j;
		int d;
	};
	struct _FORK
	{
		int i;
		int j;
		int d;
		_FORK() {i = -1; j = -1; d = -1;}
	};
	_GATE fieldHTGate[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];
	_FORK fieldNRFork[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][2];
}
namespace Hutong {
	void HTExplore(const _GATE & g, const _POINT & p);
	void NRExplore(const _FORK & f, const _POINT & p);
	void Hutong();
	void Narrow();
	Pacman::GameField * gf;
	const int DIRECTION[8][2] = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}, {1, 1}, {1, -1}, {-1, -1}, {-1, 1}};
	const int DIRECTION_T2F[4] = {2, 3, 0, 1};
	Pacman::GridStaticType fieldStatic[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH];
	inline void EdgeCorrect(int & i, int & j, const int & h, const int & w) {
		if (i == -1) {
			i = h - 1;
		}
		else if (i == h) {
			i = 0;
		}
		if (j == -1) {
			j = w - 1;
		}
		else if (j == w) {
			j = 0;
		}
	}
	void Hutong(Pacman::GameField & gameField) {
		gf = &gameField;
		memcpy(fieldStatic, gf->fieldStatic, sizeof(fieldStatic));
		Hutong();
		Narrow();
	}
	void Hutong() {
		std::queue<_POINT> qEnd;
		// find first ends
		for (int i = 0; i < gf->height; ++i) {
			for (int j = 0; j < gf->width; ++j) {
				_POINT pEnd = {i, j, -1};
				if (gf->fieldStatic[i][j] == 0xe) {
					pEnd.d = 0;
				}
				else if (gf->fieldStatic[i][j] == 0xd) {
					pEnd.d = 1;
				}
				else if (gf->fieldStatic[i][j] == 0xb) {
					pEnd.d = 2;
				}
				else if (gf->fieldStatic[i][j] == 0x7) {
					pEnd.d = 3;
				}
				if (pEnd.d >= 0) {
					pEnd.d = -1;
					qEnd.push(pEnd);
				}
			}
		}
		// extend
		while (qEnd.size()) {
			_POINT now = qEnd.front();
			qEnd.pop();
			int direction = -1;
			if (fieldStatic[now.i][now.j] == 0xe) {
				direction = 0;
			}
			else if (fieldStatic[now.i][now.j] == 0xd) {
				direction = 1;
			}
			else if (fieldStatic[now.i][now.j] == 0xb) {
				direction = 2;
			}
			else if (fieldStatic[now.i][now.j] == 0x7) {
				direction = 3;
			}
			if (direction >= 0) {
				// 解除门标记
				fieldHTGate[now.i][now.j].i = -1;
				fieldHTGate[now.i][now.j].j = -1;
				// 砌墙
				_POINT next = {now.i + DIRECTION[direction][0], now.j + DIRECTION[direction][1], direction};
				EdgeCorrect(next.i, next.j, gf->height, gf->width);
				fieldStatic[next.i][next.j] |= (Pacman::GridStaticType)(1 << DIRECTION_T2F[direction]);
				qEnd.push(next);
			}
			else {
				// 创建门标记
				fieldHTGate[now.i][now.j].i = now.i;
				fieldHTGate[now.i][now.j].j = now.j;
			}
		}
		// re-explore
		for (int i = 0; i < gf->height; ++i) {
			for (int j = 0; j < gf->width; ++j) {
				if (fieldHTGate[i][j].i == i &&
					fieldHTGate[i][j].j == j) {
					for (int d = 0; d < 4; ++d) {
						if (fieldStatic[i][j] ^ gf->fieldStatic[i][j] & (1 << d)) {
							_POINT next = {i, j, -1};
							HTExplore(fieldHTGate[i][j], next);
						}
					}
				}
			}
		}
	}
	void HTExplore(const _GATE & g, const _POINT & p) {
		fieldHTGate[p.i][p.j] = g;
		for (int d = 0; d < 4; ++d) {
			if (p.d != DIRECTION_T2F[d] && ((fieldStatic[p.i][p.j] ^ gf->fieldStatic[p.i][p.j]) & (1 << d))) {
				_POINT next = {p.i + DIRECTION[d][0], p.j + DIRECTION[d][1], d};
				EdgeCorrect(next.i, next.j, gf->height, gf->width);
				HTExplore(g, next);
			}
		}
	}
	void Narrow() {
		#ifndef _BOTZONE_ONLINE
		// std::cout << "Debug: Narrow();\n";
		#endif
		// 搜索路口
		for (int i = 0;  i < gf->height; ++i) {
			for (int j = 0; j < gf->width; ++j) {
				if (bool(fieldStatic[i][j] & 0x1) + bool(fieldStatic[i][j] & 0x2) + bool(fieldStatic[i][j] & 0x4) + bool(fieldStatic[i][j] & 0x8) < 2) {
					// 创建路口（门）
					fieldNRFork[i][j][0].i = i;
					fieldNRFork[i][j][0].j = j;
					fieldNRFork[i][j][0].d = 0;
				}
			}
		}
		// 探索
		for (int i = 0;  i < gf->height; ++i) {
			for (int j = 0; j < gf->width; ++j) {
				if (fieldNRFork[i][j][0].d == 0) {
					_POINT next = {i, j, -1};
					NRExplore(fieldNRFork[i][j][0], next);
				}
			}
		}
	}
	void NRExplore(const _FORK & f, const _POINT & p) {
		#ifndef _BOTZONE_ONLINE
		// std::cout << "Debug: NRExplore( fork::{" << f.i << ',' << f.j << ',' << f.d << "} , point::{" << p.i << ',' << p.j << "} )\n";
		#endif
		if (f.d == 0) {}
		else if (fieldNRFork[p.i][p.j][0].d == 0) {
			return;
		}
		else if (fieldNRFork[p.i][p.j][0].d == -1) {
			fieldNRFork[p.i][p.j][0] = f;
		}
		else {
			fieldNRFork[p.i][p.j][1] = f;
		}
		for (int d = 0; d < 4; ++d) {
			if (p.d != DIRECTION_T2F[d] && !(fieldStatic[p.i][p.j] & (1 << d))) {
				_POINT next = {p.i + DIRECTION[d][0], p.j + DIRECTION[d][1], d};
				EdgeCorrect(next.i, next.j, gf->height, gf->width);
				_FORK fork = f;
				++fork.d;
				NRExplore(fork, next);
			}
		}
	}
}


namespace SmartRandom {
	int RandomDepth/* = RANDOM_TURN_MAX*/;
	int RandomBlock_1 = RANDOM_BLOCK_1;
	int RandomBlock_2 = RANDOM_BLOCK_2;

	Pacman::Direction actionsList[625][MAX_PLAYER_COUNT];
	int actionsListCount = 0;
	double actionScore[625] = {0.f};

	const int DIRECTION_OPPOSIDE[5] = {0, 3, 4, 1, 2};
	const int DIRECTION_MOVE_ROW[5] = {0, -1, 0, 1, 0};
	const int DIRECTION_MOVE_COL[5] = {0, 0, 1, 0, -1};

	int fieldValidCount[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH] = {0};
	Pacman::Direction fieldValidAction[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][5];

			//  <enemy[row][col]>   <me[row][col]>  [action{0~5}]   0:Safe  1: Dangerous
	int fieldDanger[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH][5] = {0};

	void PreCompute(const Pacman::GameField & gameField) {

		int height = gameField.height;
		int width = gameField.width;
		// validAction
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				for (Pacman::Direction d = Pacman::stay; d < 4; ++d) {
					if (gameField.ActionValid(i, j, d)) {
						fieldValidAction[i][j][fieldValidCount[i][j]++] = d;
					}
				}
			}
		}
		// danger
		for (int i = 0; i < height; ++i) {
			for (int j = 0; j < width; ++j) {
				for (int d = 0; d < fieldValidCount[i][j]; ++d) {
					int _i = i + DIRECTION_MOVE_ROW[fieldValidAction[i][j][d] + 1];
					int _j = j + DIRECTION_MOVE_COL[fieldValidAction[i][j][d] + 1];
					EdgeCorrect(_i, _j, height, width);
					for (int _d = 0; _d < fieldValidCount[_i][_j]; ++_d) {
						int __a = fieldValidAction[_i][_j][_d] + 1;
						int __i = _i + DIRECTION_MOVE_ROW[__a];
						int __j = _j + DIRECTION_MOVE_COL[__a];
						EdgeCorrect(__i, __j, height, width);
						fieldDanger[i][j][__i][__j][DIRECTION_OPPOSIDE[__a]] = 1;
					}
				}
			}
		}
	}

	const time_t seed = time(0);
	std::mt19937 gen(seed);
	std::uniform_int_distribution<> dis[4] = {
		std::uniform_int_distribution<>(0, 1),
		std::uniform_int_distribution<>(0, 2),
		std::uniform_int_distribution<>(0, 3),
		std::uniform_int_distribution<>(0, 4)
	};
	inline int Random(const int & upper) {
		if (upper > 1) {
			return dis[upper - 2](gen);
		}
		return 0;
	}
}

#include <stack>
#include <queue>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <iomanip>

#define MATH_E 2.718281828459

namespace JustForLove {
	using Hutong::fieldHTGate;
	using Hutong::_GATE;
	using Hutong::_POINT;
	using Hutong::fieldNRFork;
	using Hutong::_FORK;
	const int DIRECTION[8][2] = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}, {1, 1}, {1, -1}, {-1, -1}, {-1, 1}};
	const int DIRECTION_ACTION[5][2] = {{0, 0}, {-1, 0}, {0, 1}, {1, 0}, {0, -1}};
	inline void EdgeCorrect(int & i, int & j, const int & h, const int & w) {
		if (i == -1) {
			i = h - 1;
		}
		else if (i == h) {
			i = 0;
		}
		if (j == -1) {
			j = w - 1;
		}
		else if (j == w) {
			j = 0;
		}
	}

	// inline double min(const double & x, const double & y) {
	//  return x < y ? x : y;
	// }
	// inline double max(const double & x, const double & y) {
	//  return x > y ? x : y;
	// }

	int r, c;
	int myID;

	double HutongGate(const int & i, const int & j, const bool & outOfHutong, const Pacman::GameField & gf) {
		if (fieldHTGate[i][j].i == -1 || (fieldHTGate[i][j].i == i && fieldHTGate[i][j].j == j)) {
			return 1.f;
		}
		int depth = gf.fieldDistance[i][j][fieldHTGate[i][j].i][fieldHTGate[i][j].j];
		for (int p = 0; p < MAX_PLAYER_COUNT; ++p) {
			if (gf.players[p].dead == false && gf.players[p].strength > gf.players[myID].strength
				&& (outOfHutong ? 1 : -1) * gf.fieldDistance[r][c][fieldHTGate[i][j].i][fieldHTGate[i][j].j]
					 + 2 * depth
					 >= gf.fieldDistance[gf.players[p].row][gf.players[p].col][fieldHTGate[i][j].i][fieldHTGate[i][j].j]) {
				return 0.f;
			}
		}
		return 0.7 + 0.3 / (0.9 + pow(MATH_E, depth - 1.7f));
	}

	double NarrowFork(const int & i, const int & j, const bool & inNarrow, const Pacman::GameField & gf) {
		if (fieldNRFork[i][j][1].d == -1) {
			return 1.f;
		}
		int enemyCount = 0;
		int forkState = 0;
		for (int p = 0; p < MAX_PLAYER_COUNT; ++p) {
			if (gf.players[p].dead == false && gf.players[p].strength > gf.players[myID].strength) {
				bool enemy = false;
				if (gf.fieldDistance[r][c][i][j] + fieldNRFork[i][j][0].d
					 >= gf.fieldDistance[gf.players[p].row][gf.players[p].col][fieldNRFork[i][j][0].i][fieldNRFork[i][j][0].j]) {
					enemy = true;
					forkState |= 0x1;
				}
				if (gf.fieldDistance[r][c][i][j] + fieldNRFork[i][j][1].d
					 >= gf.fieldDistance[gf.players[p].row][gf.players[p].col][fieldNRFork[i][j][1].i][fieldNRFork[i][j][1].j]) {
					enemy = true;
					forkState |= 0x2;
				}
				if (enemy) {
					++enemyCount;
				}
			}
		}
		if (forkState == 0x3 && enemyCount > 1) {
			return 0.f;
		}
		return 1.f;
	}

	double Score[5] = {0};
	bool sync = false;
	int syncc = 0;

	struct ActionCompare
	{
		bool operator()(const int & a, const int & b) {
			return Score[a] > Score[b];
		}
	};

	inline void ScoreAdd(const double & score, const unsigned int & action) {
		if (action == 0) {
			#ifndef _BOTZONE_ONLINE
			cout << "ScoreAdd: " << "-1" << ' ' << score << endl;
			#endif
			Score[0] += score;
			return;
		}
		if (action & 1) {
			#ifndef _BOTZONE_ONLINE
			cout << "ScoreAdd: " << " 0" << ' ' << score << endl;
			#endif
			Score[1] += score;
		}
		if (action & 2) {
			#ifndef _BOTZONE_ONLINE
			cout << "ScoreAdd: " << " 1" << ' ' << score << endl;
			#endif
			Score[2] += score;
		}
		if (action & 4) {
			#ifndef _BOTZONE_ONLINE
			cout << "ScoreAdd: " << " 2" << ' ' << score << endl;
			#endif
			Score[3] += score;
		}
		if (action & 8) {
			#ifndef _BOTZONE_ONLINE
			cout << "ScoreAdd: " << " 3" << ' ' << score << endl;
			#endif
			Score[4] += score;
		}
	}

	void LoveYou(Pacman::GameField & gf, int _myID) {
		myID = _myID;
		r = gf.players[myID].row;
		c = gf.players[myID].col;
		bool botOutOfHutong = (fieldHTGate[r][c].i == -1 || (fieldHTGate[r][c].i == r && fieldHTGate[r][c].j == c)) ? true : false;
		bool botInNarrow = (fieldNRFork[r][c][1].d != -1) ? true : false;

		int NewFruitsTime = (gf.GENERATOR_INTERVAL + gf.turnID - 1) % gf.GENERATOR_INTERVAL + 1;

		int fieldNewFruits[FIELD_MAX_HEIGHT][FIELD_MAX_WIDTH] = {0};

		for (int i = 0; i < gf.height; ++i) {
			for (int j = 0; j < gf.width; ++j) {
				if (gf.fieldStatic[i][j] & Pacman::generator) {
					for (int n = 0; n < 8; ++n) {
						int ii = i + DIRECTION[n][0];
						int jj = j + DIRECTION[n][1];
						EdgeCorrect(ii, jj, gf.height, gf.width);
						if (gf.fieldPath[r][c][ii][jj] != -1) {
							if (!(gf.fieldContent[ii][jj] & 48)) {
								fieldNewFruits[ii][jj] = gf.GENERATOR_INTERVAL - NewFruitsTime;
							}
						}
					}
				}
			}
		}
		for (int i = 0; i < gf.height; ++i) {
			for (int j = 0; j < gf.width; ++j) {
				double hutongGate = HutongGate(i, j, botOutOfHutong, gf);
				double narrowFork = NarrowFork(i, j, botInNarrow, gf);
				if (hutongGate == 0.f || narrowFork == 0.f) {
					continue;
				}
				bool BelongToEnemy = false;
				double distanceE1 = 1.1f / (gf.fieldDistance[r][c][i][j] + 1e-1);
				double distancee1 = 1.1f / (gf.fieldDistance[r][c][i][j] + 2e-1);
				// double distanceE2 = 2.0f / (fieldNewFruits[i][j] * (gf.fieldDistance[r][c][i][j] + 1e0));
				// for (int p = 0; p < MAX_PLAYER_COUNT; ++p) {
				// 	if (gf.fieldDistance[gf.players[p].row][gf.players[p].col][i][j] < gf.fieldDistance[r][c][i][j]) {
						// BelongToEnemy = true;
				// 		break;
				// 	}
				// }
				// if (fieldNewFruits[i][j]) {
				// 	#ifndef _BOTZONE_ONLINE
				// 	cout << 'F' << '(' << i << ',' << j << ')' << gf.fieldDistance[r][c][i][j] << endl;
				// 	#endif
				// 	ScoreAdd(hutongGate*narrowFork*(BelongToEnemy?1:2)*5e1 * distanceE2, gf.fieldAction[r][c][i][j]);
				// }
				if (gf.fieldContent[i][j] & 32) {
					#ifndef _BOTZONE_ONLINE
					cout << 'B' << '(' << i << ',' << j << ')' << gf.fieldDistance[r][c][i][j] << endl;
					#endif
					ScoreAdd(hutongGate*narrowFork*(BelongToEnemy?1:2)*5e2 * distancee1 * distancee1, gf.fieldAction[r][c][i][j]);
				}
				if (gf.fieldContent[i][j] & 16) {
					#ifndef _BOTZONE_ONLINE
					cout << 'S' << '(' << i << ',' << j << ')' << gf.fieldDistance[r][c][i][j] << endl;
					#endif
					ScoreAdd(hutongGate*narrowFork*(BelongToEnemy?1:2)*5e2 * distanceE1 * distanceE1, gf.fieldAction[r][c][i][j]);
				}
			}
		}
		for (int i = 0; i < MAX_PLAYER_COUNT; ++i) {
			if (i != myID && gf.players[i].dead == false) {
				if (gf.players[i].strength > gf.players[myID].strength) {
					for (int d = 0; d < 5; ++d) {
						if (d == 0 || ~gf.fieldStatic[gf.players[i].row][gf.players[i].col] & (1 << (d - 1))) {
							int ii = gf.players[i].row + DIRECTION_ACTION[d][0];
							int jj = gf.players[i].col + DIRECTION_ACTION[d][1];
							EdgeCorrect(ii, jj, gf.height, gf.width);
							for (int a = 0; a < 5; ++a) {
								int rr = r + DIRECTION_ACTION[a][0];
								int cc = c + DIRECTION_ACTION[a][1];
								EdgeCorrect(rr, cc, gf.height, gf.width);
								if (rr == ii && cc == jj) {
									#ifndef _BOTZONE_ONLINE
									cout << 'E' << '(' << ii << ',' << jj << ')' << gf.fieldDistance[r][c][ii][jj] << endl;
									cout << "ScoreEnemy: " << a << endl;
									#endif
									Score[a] -= 3e15;
								}
							}
						}
					}
				}
				else if (gf.players[i].strength == gf.players[myID].strength &&
					gf.players[i].row == r && gf.players[i].col == c) {
						if (bot::data.IsExist("syncc")) {
							bot::data.GetBase64("syncc", &syncc);
						}
						if (sync == false) {
							++syncc;
							sync = true;
						}
				}
				else if (gf.players[i].strength < gf.players[myID].strength) {
					double hutongGate = HutongGate(gf.players[i].row, gf.players[i].col, botOutOfHutong, gf);
					if (hutongGate != 0.f && hutongGate != 1.f) {
						int rr = gf.players[i].row;
						int cc = gf.players[i].col;
						int ii = fieldHTGate[rr][cc].i;
						int jj = fieldHTGate[rr][cc].j;
						if (gf.fieldDistance[r][c][ii][jj] < gf.fieldDistance[rr][cc][ii][jj]) {
							ScoreAdd(1.1e3 * hutongGate * (gf.players[i].strength / 2), gf.fieldAction[r][c][rr][cc]);
						}
					}
				}
			}
		}

		// all zero hutong
		// bool zero = true;
		// for (int i = 0; i < 5; ++i) {
		// 	if (Score[i] > 0) {
		// 		zero = false;
		// 		break;
		// 	}
		// }
		// if (zero) {
		// 	for (int a = 0; a < 5; ++a) {
		// 		if (a == 0 || ~gf.fieldStatic[r][c] & (1 << (a - 1))) {
		// 			int i = r + DIRECTION_ACTION[a][0];
		// 			int j = c + DIRECTION_ACTION[a][1];
		// 			EdgeCorrect(i, j, gf.height, gf.width);
		// 			if (fieldHTGate[i][j].i != -1 && (fieldHTGate[i][j].i != i || fieldHTGate[i][j].j != j)) {
		// 				Score[a] -= 1e10;
		// 			}
		// 		}
		// 	}
		// }


		// if (bot::data.IsExist("last-action")) {
		// 	int last_action;
		// 	bot::data.GetBase64("last-action", &last_action);
		// 	if(last_action >= 0) {
		// 		Score[(last_action + 2) % 4 + 1] -= 10;
		// 	}
		// }
		#ifndef _BOTZONE_ONLINE
		for (int i = 0; i < 5; ++i) {
			cout << std::setw(3) << (i - 1) << ": " << Score[i] << endl;
		}
		#endif

		bot::data.SetBase64("syncc", &syncc, sizeof(syncc));
		for (int i = 0; i < 5; ++i) {
			std::ostringstream s;
			s << std::setprecision(10) << Score[i];
			bot::Debug["score"]["0:Love"][i] = (Json::Value)(s.str());
		}
	}

	// 决策 估值、决策分离
	int IWill(Pacman::GameField & gf) {
		std::vector<int> actions;
		if (!(sync && syncc >= 2)) {
			actions.push_back(0);
			for (int a = 1; a <= 4; ++a) {
				if (gf.fieldStatic[r][c] & (1 << (a - 1))) {
					continue;
				}
				if (Score[a] < Score[actions.front()]) {
					continue;
				}
				if (Score[a] > Score[actions.front()]) {
					actions.clear();
				}
				actions.push_back(a);
			}
		}
		else {
			// Hutong special
			for (int a = 0; a <= 4; ++a) {
				int i = r + DIRECTION_ACTION[a][0];
				int j = c + DIRECTION_ACTION[a][1];
				EdgeCorrect(i, j, gf.height, gf.width);
				if (fieldHTGate[i][j].i != -1 && (fieldHTGate[i][j].i != i || fieldHTGate[i][j].j != j)) {
					Score[a] -= 1e10;
				}
				if (Score[a] > 0) {
					actions.push_back(a);
				}
			}
			if (actions.empty()) {
				actions.push_back(0);
			}
			else {
				std::sort(actions.begin(), actions.end(), ActionCompare());
				while (actions.size() > 2) {
					actions.pop_back();
				}
			}
		}
		for (int i = 0; i < 5; ++i) {
			std::ostringstream s;
			s << std::setprecision(10) << Score[i];
			bot::Debug["score"]["2:Final"][i] = (Json::Value)(s.str());
		}
		if (actions.size()) {
			return actions[SmartRandom::Random(actions.size())] - 1;
			// return actions[rand() % actions.size()] - 1;
		}
		else {
			return 5;
		}
	}
}

// 一些辅助程序
namespace Helpers
{

	inline int RandBetween(int a, int b)
	{
		if (a > b)
			swap(a, b);
		return rand() % (b - a) + a;
	}

	void RandomPlay(Pacman::GameField &gameField, int actionsListIndex, int myID)
	{
		int count = 0;

		for (int p = 0; p < MAX_PLAYER_COUNT; ++p) {
			if (gameField.players[p].dead)
				continue;
			gameField.actions[p] = SmartRandom::actionsList[actionsListIndex][p];
		}

		// 演算一步局面变化
		// NextTurn返回true表示游戏没有结束
		count++;

		if (gameField.NextTurn()) {
			while (true)
			{
				// 对每个玩家生成随机的合法动作
				for (int i = 0; i < MAX_PLAYER_COUNT; i++)
				{
					Pacman::Player & p = gameField.players[i];
					if (p.dead)
						continue;

					Pacman::Direction * valid = SmartRandom::fieldValidAction[p.row][p.col];
					int vCount = SmartRandom::fieldValidCount[p.row][p.col];

					Pacman::Direction valid2[5];
					int v2Count = 0;
					for (int d = 0; d < vCount; ++d) {
						int Dangerous = 0;
						for (int p_i = 0; p_i < MAX_PLAYER_COUNT; ++p_i) {
							Pacman::Player & p_enemy = gameField.players[p_i];
							if (p_enemy.dead || p_enemy.strength <= p.strength) {
								continue;
							}
							if (SmartRandom::fieldDanger[p_enemy.row][p_enemy.col][p.row][p.col][valid[d] + 1]) {
								Dangerous = 1;
								break;
							}
						}
						if (!Dangerous) {
							valid2[v2Count++] = valid[d];
						}
					}
					if (v2Count) {
						vCount = v2Count;
						valid = valid2;
					}

					gameField.actions[i] = valid[SmartRandom::Random(vCount)];
				}

				// 演算一步局面变化
				// NextTurn返回true表示游戏没有结束
				bool hasNext = gameField.NextTurn();
				count++;

				if (!hasNext || count >= SmartRandom::RandomDepth)
					break;
			}
		}


		// 计算分数

		int total = 0;
		for (int _ = 0; _ < MAX_PLAYER_COUNT; _++)
			total += gameField.players[_].strength;

		SmartRandom::actionScore[actionsListIndex] += double(100 * gameField.players[myID].strength) / total;

		// 老版计算分数改编 用于10k分的排名分
		int rank2player[] = { 0, 1, 2, 3 };
		for (int j = 0; j < MAX_PLAYER_COUNT; j++)
			for (int k = 0; k < MAX_PLAYER_COUNT - j - 1; k++)
				if (gameField.players[rank2player[k]].strength > gameField.players[rank2player[k + 1]].strength)
					swap(rank2player[k], rank2player[k + 1]);
 
		int scorebase = 1;
		if (rank2player[0] == myID)
			SmartRandom::actionScore[actionsListIndex] += 10;
		else
			for (int j = 1; j < MAX_PLAYER_COUNT; j++)
			{
				if (gameField.players[rank2player[j - 1]].strength < gameField.players[rank2player[j]].strength)
					scorebase = j + 1;
				if (rank2player[j] == myID)
				{
					SmartRandom::actionScore[actionsListIndex] += scorebase * 10;
					break;
				}
			}

		// 恢复游戏状态到最初（就是本回合）
		while (count-- > 0)
			gameField.PopState();
	}
}

int main()
{
	

	clock_t start_time = clock();
	clock_t end_time;
	double run_time;

	Pacman::GameField gameField;
	string data, globalData; // 这是回合之间可以传递的信息

	// 如果在本地调试，有input.txt则会读取文件内容作为输入
	// 如果在平台上，则不会去检查有无input.txt
	int myID = gameField.ReadInput("input.txt", data, globalData); // 输入，并获得自己ID
	srand(Pacman::seed + myID);

	// preLoad
	unsigned int nRound = 0;
	if (bot::data.IsExist("Distance") == false) {
		if (bot::globalData.IsExist("Round")) {
			bot::globalData.GetBase64("Round", &nRound);
		}
		++nRound;
		bot::globalData.SetBase64("Round", &nRound, sizeof(nRound));

		path::Dijkstra(gameField);
		Hutong::Hutong(gameField);
		bot::data.SetBase64("Distance", gameField.fieldDistance, sizeof(gameField.fieldDistance));
		bot::data.SetBase64("Path", gameField.fieldPath, sizeof(gameField.fieldPath));
		bot::data.SetBase64("Action", gameField.fieldAction, sizeof(gameField.fieldAction));
		bot::data.SetBase64("Hutong", Hutong::fieldHTGate, sizeof(Hutong::fieldHTGate));
		bot::data.SetBase64("Narrow", Hutong::fieldNRFork, sizeof(Hutong::fieldNRFork));
	}

	// BotJson
	bot::data.GetBase64("Distance", gameField.fieldDistance);
	bot::data.GetBase64("Path", gameField.fieldPath);
	bot::data.GetBase64("Action", gameField.fieldAction);
	bot::data.GetBase64("Hutong", Hutong::fieldHTGate);
	bot::data.GetBase64("Narrow", Hutong::fieldNRFork);

	// JustForLove
	JustForLove::LoveYou(gameField, myID);

	// 高级随机，看哪个动作随机赢得最多
	SmartRandom::PreCompute(gameField);

	int vCount[MAX_PLAYER_COUNT] = {0};
	int v2Count[MAX_PLAYER_COUNT] = {0};
	Pacman::Direction * valid[MAX_PLAYER_COUNT];
	Pacman::Direction valid2[MAX_PLAYER_COUNT][5];
	
	for (int p = 0; p < MAX_PLAYER_COUNT; ++p) {

		valid[p] = nullptr;
		Pacman::Player & player = gameField.players[p];
		if (player.dead)
			continue;
		vCount[p] = SmartRandom::fieldValidCount[player.row][player.col];
		valid[p] = SmartRandom::fieldValidAction[player.row][player.col];

		for (int d = 0; d < vCount[p]; ++d) {
			int Dangerous = 0;
			for (int p_i = 0; p_i < MAX_PLAYER_COUNT; ++p_i) {
				Pacman::Player & p_enemy = gameField.players[p_i];
				if (p_enemy.dead || p_enemy.strength <= player.strength) {
					continue;
				}
				if (SmartRandom::fieldDanger[p_enemy.row][p_enemy.col][player.row][player.col][valid[p][d] + 1]) {
					Dangerous = 1;
					break;
				}
			}
			if (!Dangerous) {
				valid2[p][v2Count[p]++] = valid[p][d];
			}
		}

		if (v2Count[p]) {
			vCount[p] = v2Count[p];
			valid[p] = valid2[p];
		}
	}
	
	int actionsIndex[MAX_PLAYER_COUNT] = {0};

	using SmartRandom::actionsListCount;
	using SmartRandom::actionsList;

	for (;;) {
		for (int p = 0; p < MAX_PLAYER_COUNT; ++p) {
			Pacman::Player & player = gameField.players[p];
			if (player.dead) {
				continue;
			}
			actionsList[actionsListCount][p] = valid[p][actionsIndex[p]];
		}
		++actionsListCount;
		bool enumHead = true;
		for (int p = 0; p < MAX_PLAYER_COUNT - 1; ++p) {
			if (enumHead) {
				++actionsIndex[p];
				enumHead = false;
			}
			if (actionsIndex[p] > 0 && actionsIndex[p] >= vCount[p]) {
				actionsIndex[p] = 0;
				++actionsIndex[p + 1];
			}
		}
		if (actionsIndex[MAX_PLAYER_COUNT - 1] > 0  && actionsIndex[MAX_PLAYER_COUNT - 1] >= vCount[MAX_PLAYER_COUNT - 1]) {
			break;
		}
	}

	SmartRandom::RandomBlock_1 /= actionsListCount;
	SmartRandom::RandomBlock_2 /= actionsListCount;
	SmartRandom::RandomDepth = 12;
	
	end_time = clock();
	run_time = static_cast<double>(end_time - start_time) / CLOCKS_PER_SEC * 1000;
	{
		std::ostringstream s;
		s << std::setprecision(10) << run_time << " ms";
		bot::Debug["time"]["0:Random-Start"] = (Json::Value)(s.str());
	}

	int BlockCount1 = 0;

	while (run_time < RANDOM_CLOCK_1) {
		++BlockCount1;
		// ++SmartRandom::RandomDepth;
		for (int i = 0; i < actionsListCount; ++i) {
			for (int t = 0; t < SmartRandom::RandomBlock_1; ++t) {
				Helpers::RandomPlay(gameField, i, myID);
			}
		}

		end_time = clock();
		run_time = static_cast<double>(end_time - start_time) / CLOCKS_PER_SEC * 1000;
		SmartRandom::gen.seed(end_time);
	}
	{
		std::ostringstream s;
		s << std::setprecision(10) << run_time << " ms";
		bot::Debug["time"]["1:Block-Stop-1"] = (Json::Value)(s.str());
		bot::Debug["count"]["1:Random-Block-1"] = (Json::Value)(BlockCount1);
		bot::Debug["count"]["1:Random-Total-Block-1"] = (Json::Value)(BlockCount1 * SmartRandom::RandomBlock_1 * actionsListCount);
	}
	int BlockCount2 = 0;

	while (run_time < RANDOM_CLOCK_2) {
		++BlockCount2;
		++SmartRandom::RandomDepth;
		for (int i = 0; i < actionsListCount; ++i) {
			for (int t = 0; t < SmartRandom::RandomBlock_2; ++t) {
				Helpers::RandomPlay(gameField, i, myID);
			}
		}

		end_time = clock();
		run_time = static_cast<double>(end_time - start_time) / CLOCKS_PER_SEC * 1000;
		SmartRandom::gen.seed(end_time);
	}
	{
		std::ostringstream s;
		s << std::setprecision(10) << run_time << " ms";
		bot::Debug["time"]["2:Block-Stop-2"] = (Json::Value)(s.str());
		bot::Debug["count"]["2:Random-Block-2"] = (Json::Value)(BlockCount2);
		bot::Debug["count"]["2:Random-Total-Block-2"] = (Json::Value)(BlockCount2 * SmartRandom::RandomBlock_2 * actionsListCount);
		bot::Debug["count"]["3:Random-Total"] = (Json::Value)(BlockCount1 * SmartRandom::RandomBlock_1 * actionsListCount + BlockCount2 * SmartRandom::RandomBlock_2 * actionsListCount);
	}
	
	// 博弈
	double minMaxScore[5];
	for (int i = 0; i < 5; ++i) {
		minMaxScore[i] = DBL_MIN;
	}

	for (int i = 0; i < vCount[myID]; ++i) {
		minMaxScore[valid[myID][i] + 1] = DBL_MAX;
	}

	for (int i = 0; i < actionsListCount; ++i) {
		double score = SmartRandom::actionScore[i];
		int myAct = SmartRandom::actionsList[i][myID] + 1;
		if (minMaxScore[myAct] > score) {
			minMaxScore[myAct] = score;
		}
	}

	for (int i = 0; i < 5; ++i) {
		std::ostringstream s;
		s << std::setprecision(10) << minMaxScore[i];
		bot::Debug["score"]["1:Random"][i] = (Json::Value)(s.str());
	}

	double max = 0;
	for (int i = 0; i < 5; ++i) {
		if (JustForLove::Score[i] > max) {
			max = JustForLove::Score[i];
		}
	}

	for (int i = 0; i < 5; ++i) {
		if (JustForLove::Score[i] >= 0) {
			JustForLove::Score[i] += 10 * max;
		}
	}

	for (int i = 0; i < 5; ++i) {
		std::ostringstream s;
		s << std::setprecision(10) << JustForLove::Score[i];
		bot::Debug["score"]["0:JustForLove-corrected"][i] = (Json::Value)(s.str());
	}

	double PositiveScoreAve = 0;
	int PositiveScoreCount = 0;
	for (int i = 0; i < 5; ++i) {
		if (JustForLove::Score[i] > 0) {
			++PositiveScoreCount;
			PositiveScoreAve += sqrt(JustForLove::Score[i]);
			if (minMaxScore[i] > 0) {
				JustForLove::Score[i] *= minMaxScore[i];
			}
		}
	}

	if (PositiveScoreCount == 0) {
		for (int i = 0; i < 5; ++i) {
			if (JustForLove::Score[i] == 0) {
				if (minMaxScore[i] > 0) {
					JustForLove::Score[i] = minMaxScore[i];
				}
			}
		}
	}

	#ifndef _BOTZONE_ONLINE
	// debug
	for (int i = 0; i < actionsListCount; ++i) {
		printf("actionsList %d\n", i);
		for (int p = 0; p < MAX_PLAYER_COUNT; ++p) {
			printf("  %d", (int)actionsList[i][p]);
		}
		printf("  %lf\n", SmartRandom::actionScore[i]);
	}
	for (int i = 0; i < 5; ++i) {
		printf(" %lf\n", minMaxScore[i]);
	}
	#endif

	// 输出当前游戏局面状态以供本地调试。注意提交到平台上会自动优化掉，不必担心。
	gameField.DebugPrint();

	// JustForLove
	int maxD = JustForLove::IWill(gameField);
	bot::data.SetBase64("last-action", &maxD, sizeof(maxD));
	bot::data.Write();
	bot::globalData.Write();

	if (maxD == 5) {
		maxD = valid[myID][SmartRandom::Random(vCount[myID])];
	}

	const string strPoem = "[\"舒婷的诗\",\"舒婷的诗\",\"\",\"双桅船\",\"\",\"雾打湿了我的双翼\",\"可风却不容我再迟疑\",\"岸呵，心爱的岸\",\"昨天刚刚和你告别\",\"今天你又在这里\",\"明天我们将在\",\"另一个维度相遇\",\"\",\"是一场风暴、一盏灯\",\"把我们联系在一起\",\"是一场风暴、另一盏灯\",\"使我们再分东西\",\"不怕天涯海角\",\"岂在朝朝夕夕\",\"你在我的航程上\",\"我在你的视线里\",\"\",\"\",\"致橡树\",\"\",\"我如果爱你——\",\"绝不像攀援的凌霄花，\",\"借你的高枝炫耀自己；\",\"我如果爱你——\",\"绝不学痴情的鸟儿，\",\"为绿荫重复单调的歌曲；\",\"也不止像泉源，\",\"常年送来清凉的慰藉；\",\"也不止像险峰，\",\"增加你的高度，衬托你的威仪。\",\"甚至日光，\",\"甚至春雨。\",\"不，这些都还不够！\",\"我必须是你近旁的一株木棉，\",\"作为树的形象和你站在一起。\",\"根，紧握在地下；\",\"叶，相触在云里。\",\"每一阵风过，\",\"我们都互相致意，\",\"但没有人，\",\"听懂我们的言语。\",\"你有你的铜枝铁干，\",\"像刀，像剑，\",\"也像戟；\",\"我有我红硕的花朵，\",\"像沉重的叹息，\",\"又像英勇的火炬。\",\"我们分担寒潮、风雷、霹雳；\",\"我们共享雾霭、流岚、虹霓。\",\"仿佛永远分离，\",\"却又终身相依。\",\"这才是伟大的爱情，\",\"坚贞就在这里：\",\"爱——\",\"不仅爱你伟岸的身躯，\",\"也爱你坚持的位置，\",\"足下的土地。\",\"\",\"\",\"神女峰\",\"\",\"在向你挥舞的各色花帕中\",\"是谁的手突然收回\",\"紧紧捂住了自己的眼睛\",\"当人们四散离去，谁\",\"还站在船尾\",\"衣裙漫飞，如翻涌不息的云\",\"江涛　　　　　\",\"　　高一声　　\",\"　　　　低一声\",\"\",\"美丽的梦留下美丽的忧伤\",\"人间天上，代代相传\",\"但是，心\",\"真能变成石头吗\",\"为眺望远天的杳鹤\",\"错过无数次春江月明\",\"\",\"沿着江岸\",\"金光菊和女贞子的洪流\",\"正煽动新的背叛\",\"　　与其在悬崖上展览千年\",\"　　不如在爱人肩头痛哭一晚\"]\"";
	Json::Value jsonPoem; Json::Reader().parse(strPoem, jsonPoem);
	std::string strLine;
	if (gameField.turnID < jsonPoem.size()) {
	 	strLine = jsonPoem[(Json::Value::UInt) gameField.turnID].asString();
	}

	end_time = clock();
	run_time = static_cast<double>(end_time - start_time) / CLOCKS_PER_SEC * 1000;
	SmartRandom::gen.seed(end_time);
	{
		std::ostringstream s;
		s << std::setprecision(10) << run_time << " ms";
		bot::Debug["time"]["3:Final"] = (Json::Value)(s.str());
	}

	#ifdef _DEBUG_CRY
	gameField.WriteOutput((Pacman::Direction)maxD, std::string("t:") + std::to_string(run_time), data, globalData);
	#else
	gameField.WriteOutput((Pacman::Direction)maxD, strLine, data, globalData);
	#endif
	return 0;
}